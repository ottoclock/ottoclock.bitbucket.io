var elem = document.getElementById('draw-shapes');
var Frame_width = 800;
var Frame_height = 800;
var params = { width: Frame_width, height: Frame_height, type: Two.Types.canvas};
var two = new Two(params).appendTo(elem);

var background = two.makeRectangle(0,0, Frame_width*2, Frame_height*2);
background.fill ='rgb(0, 0, 0)';
background.noStroke();


// var stars  = two.makeGroup();
// two.add(stars);


var starspositionRadius = 1;
var circleR1Min = 40;
var circleR1Max = 200;
var circleR1Multiplier = 5; // Move to next circle. the higher the less dense
var stepMultiplier = 100;
var stepMin = 1.5;
var particleNumber = 0;

while (starspositionRadius < Math.sqrt(Frame_width*Frame_width + Frame_height*Frame_height)/2){

  //Gap Size

  if (Math.random()< 0.1){
    starspositionRadius += Math.random()*(circleR1Max - circleR1Min) + circleR1Min; //makes empty gap
  } else{
     starspositionRadius += Math.abs(gaussianRandom()*circleR1Multiplier); //move to next ring
  }

  // Setting random brightness and random step size

  var starStep =  stepMultiplier*Math.random() / starspositionRadius;
  var starBrighness = Math.random()*255; //compressing the dynamic range to avoid creating points that are not visible (too dark)
  var currentStep = 0;

  // Placing stars within one starspositionRadius
  while (currentStep < 2*Math.PI) {
    currentStep += starStep;
    addnewStar(starspositionRadius, currentStep,starBrighness);
    particleNumber +=1;
  }
}
console.log(particleNumber);

// two.bind('update' , function(frameCount){
// }).play();

two.update();




function addnewStar(radius, angle, brightness){
  var x = Frame_width/2 + radius*Math.cos(angle);
  var y = Frame_height/2 - radius*Math.sin(angle);


  var inCircle = two.makePolygon(x,y,1.5,3);
  var brightnessstring = brightness.toString();

  inCircle.fill = 'rgb('+ brightnessstring + ',' + brightnessstring + ',' + brightnessstring + ')';
  inCircle.noStroke();
  inCircle.opacity = 0.8 ;
}

function gaussianRandom(mean = 0, sd = 1) {
  //Copied from https://github.com/processing/p5.js/blob/9cb1d13c70730bd9671a379219369faddf407573/src/math/random.js

  var y1,x1, x2, w;
  do {
    x1 = Math.random()*2 - 1;
    x2 = Math.random()*2 - 1;
    w = x1 * x1 + x2 * x2;
  } while (w >= 1);

  w = Math.sqrt(-2 * Math.log(w) / w);
  y1 = x1 * w;
  return y1 * sd + mean;
}
